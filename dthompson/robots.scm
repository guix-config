(define-module (dthompson robots)
  #:use-module (dthompson json)
  #:export (load-robots-json))

;; Data from https://github.com/ai-robots-txt/ai.robots.txt/blob/main/robots.json
(define %robots-json
  (string-append (dirname (current-filename)) "/robots.json"))

;; Bots that aren't in the above JSON.
(define %additional-robots '("MJ12bot"))

(define* (load-robots-json #:optional (file-name %robots-json))
  (append (map car (call-with-input-file file-name read-json))
          %additional-robots))
