;;; Copyright © 2022-2024 David Thompson <dthompson2@worcester.edu>
;;;
;;; This program is free software; you can redistribute it and/or
;;; modify it under the terms of the GNU General Public License as
;;; published by the Free Software Foundation; either version 3 of the
;;; License, or (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see
;;; <http://www.gnu.org/licenses/>.

;;; Commentary:
;;
;; DAW laptop configuration.
;;
;; To update the OS configuration, run:
;;
;;    guix system reconfigure -L . -e '(@ (dthompson machines rise) rise-os)'
;;
;; To update the home environment, run:
;;
;;   guix home reconfigure -L . -e '(@ (dthompson machines rise) rise-home)'

(define-module (dthompson machines rise)
  #:use-module (dthompson home common)
  #:use-module (dthompson home pipewire)
  #:use-module (gnu)
  #:use-module (gnu bootloader)
  #:use-module (gnu home)
  #:use-module (gnu home services sound)
  #:use-module (gnu packages)
  #:use-module (gnu services)
  #:use-module (gnu services base)
  #:use-module (gnu services cups)
  #:use-module (gnu services desktop)
  #:use-module (gnu services games)
  #:use-module (gnu services pm)
  #:use-module (gnu services virtualization)
  #:use-module (gnu services sound)
  #:use-module (gnu services xorg)
  #:use-module (gnu system)
  #:use-module (gnu system accounts)
  #:use-module (gnu system file-systems)
  #:use-module (gnu system mapped-devices)
  #:use-module (nongnu packages linux)
  #:use-module (nongnu system linux-initrd)
  #:export (rise-os rise-home))

(define rise-os
  (operating-system
   (locale "en_US.utf8")
   (timezone "America/New_York")
   (keyboard-layout (keyboard-layout "us"))
   (host-name "rise")
   (bootloader (bootloader-configuration
                (bootloader grub-efi-bootloader)
                (targets (list "/boot/efi"))
                (keyboard-layout keyboard-layout)))
   ;; Use regular Linux with the big bad proprietary firmware blobs.
   (kernel linux)
   ;; Install microcode updates because why the heck wouldn't you want
   ;; this???
   (initrd microcode-initrd)
   ;; sof-firmware is required for sound to work, linux-firmware takes
   ;; care of everything else.
   (firmware (list sof-firmware linux-firmware))
   (users (cons* (user-account
                  (name "dave")
                  (comment "David Thompson")
                  (group "users")
                  (home-directory "/home/dave")
                  (supplementary-groups
                   '("audio"
                     "kvm"      ; for VMs
                     "lp"       ; for bluetooth
                     "netdev"
		     "realtime" ; for realtime prioritization
                     "video"
                     "wheel"))) ; for sudo
                 %base-user-accounts))
   (groups (cons (user-group
		  (name "realtime")
		  (system? #t))
	         %base-groups))
   (packages (cons (specification->package "nss-certs")
                   %base-packages))
   (services (modify-services
	         (cons* (service gnome-desktop-service-type)
                        (service bluetooth-service-type)
		        ;; Keep CPU temp in check.
                        (service thermald-service-type)
		        ;; Enable CUPS web UI and also add the extension
		        ;; for my Brother laser printer.
                        (service cups-service-type
                                 (cups-configuration
                                  (web-interface? #t)
                                  (extensions
			           (map specification->package '("cups-filters" "brlaser")))))
		        ;; The first entry increases the maximum realtime
		        ;; priority for non-privileged processes; the
		        ;; second entry lifts any restriction of the
		        ;; maximum address space that can be locked in
		        ;; memory.
		        (service pam-limits-service-type
			         (list
			          (pam-limits-entry "@realtime"
					            'both 'rtprio 99)
			          (pam-limits-entry "@realtime"
					            'both 'memlock 'unlimited)))
                        %desktop-services)
	       ;; In this house, we use Pipewire.
	       (delete pulseaudio-service-type)
               ;; Get nonguix substitutes.
               (guix-service-type config =>
                                  (guix-configuration
                                   (inherit config)
                                   (substitute-urls
                                    (append (list "https://substitutes.nonguix.org")
                                            %default-substitute-urls))
                                   (authorized-keys
                                    (append (list (local-file "../../keys/nonguix-signing-key.pub"))
                                            %default-authorized-guix-keys))))))
   (mapped-devices (list (mapped-device
                          (source (uuid "ee0a37d1-36d6-4e50-8af0-fc0bfe8c22ef"))
                          (target "cryptroot")
                          (type luks-device-mapping))))
   (file-systems (cons* (file-system
                         (mount-point "/boot/efi")
                         (device (uuid "1F21-80A1" 'fat32))
                         (type "vfat"))
                        (file-system
                         (mount-point "/")
                         (device "/dev/mapper/cryptroot")
                         (type "ext4")
                         (dependencies mapped-devices)) %base-file-systems))
   (name-service-switch %mdns-host-lookup-nss)))

(define rise-home
  (home-environment
   (packages
    (cons* (package-with-pw-jack (specification->package "ardour") "ardour8")
           (package-with-pw-wrapper (specification->package "firefox"))
           (specifications->packages
            '("audacity"
              "blender"
              "calf"
              "carla"
              "dragonfly-reverb"
              "easyeffects"
              "emacs"
              "emacs-better-defaults"
              "emacs-buffer-env"
              "emacs-doom-modeline"
              "emacs-doom-themes"
              "emacs-flycheck-guile"
              "emacs-geiser-guile"
              "emacs-magit"
              "emacs-markdown-mode"
              "emacs-paredit"
              "emacs-rainbow-delimiters"
              "emacs-smex"
              "emacs-use-package"
              "emacs-which-key"
              "font-google-noto-emoji"
              "font-google-noto-sans-cjk"
              "font-google-noto-serif-cjk"
              "font-inconsolata"
              "gimp"
              "git"
              "gnome-tweaks"
              "google-chrome-stable"
              "htop"
              "libresprite"
              "lsp-plugins"
              "milkytracker"
              "ncurses"
              "obs"
              "openssh"
              "pavucontrol"
              "qjackctl"
              "qpwgraph"
              "sfxr"
              "strace"
              "x42-plugins"
              "xdg-desktop-portal"
              "xdg-desktop-portal-gtk"
              "yt-dlp"))))
   (services (cons (service home-pipewire-service-type)
                   common-services))))
