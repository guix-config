;;; Copyright © 2022-2024 David Thompson <dthompson2@worcester.edu>
;;;
;;; This program is free software; you can redistribute it and/or
;;; modify it under the terms of the GNU General Public License as
;;; published by the Free Software Foundation; either version 3 of the
;;; License, or (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see
;;; <http://www.gnu.org/licenses/>.

;;; Commentary:
;;
;; Laptop OS configuration.
;;
;; To update the OS, run:
;;
;;   sudo -E guix system reconfigure -L . -e '(@ (dthompson machines ikaruga) ikaruga-os)'
;;
;; To update the home environment, run:
;;
;;   guix home reconfigure -L . -e '(@ (dthompson machines ikaruga) ikaruga-home)'

(define-module (dthompson machines ikaruga)
  #:use-module (dthompson home common)
  #:use-module (dthompson home pipewire)
  #:use-module (gnu)
  #:use-module (gnu bootloader)
  #:use-module (gnu home)
  #:use-module (gnu home services sound)
  #:use-module (gnu packages)
  #:use-module (gnu services)
  #:use-module (gnu services base)
  #:use-module (gnu services cups)
  #:use-module (gnu services desktop)
  #:use-module (gnu services games)
  #:use-module (gnu services pm)
  #:use-module (gnu services virtualization)
  #:use-module (gnu services sound)
  #:use-module (gnu services xorg)
  #:use-module (gnu system)
  #:use-module (gnu system accounts)
  #:use-module (gnu system file-systems)
  #:use-module (gnu system mapped-devices)
  #:use-module (nongnu packages linux)
  #:use-module (nongnu system linux-initrd))

(define-public ikaruga-os
  (operating-system
   (locale "en_US.utf8")
   (timezone "America/New_York")
   (keyboard-layout (keyboard-layout "us"))
   (host-name "ikaruga")
   (bootloader (bootloader-configuration
                (bootloader grub-efi-bootloader)
                (targets (list "/boot/efi"))
                (keyboard-layout keyboard-layout)))
   ;; Use regular Linux with the big bad proprietary firmware blobs.
   (kernel linux)
   (initrd microcode-initrd)
   ;; sof-firmware is required for sound to work, linux-firmware takes
   ;; care of everything else.
   (firmware (list sof-firmware linux-firmware))
   (users (cons* (user-account
                  (name "dave")
                  (comment "David Thompson")
                  (group "users")
                  (home-directory "/home/dave")
                  (supplementary-groups
                   '("audio"
                     "kvm"              ; for running VMs
                     "libvirt"          ; also for VMs
                     "lp"               ; for bluetooth
                     "netdev"
                     "realtime"         ; for realtime prioritization
                     "video"
                     "wheel")))         ; for sudo
                 %base-user-accounts))
   (groups (cons (user-group
        	  (name "realtime")
        	  (system? #t))
                 %base-groups))
   (packages (cons (specification->package "nss-certs")
                   %base-packages))
   (services
    (let ((services
           (cons* (service gnome-desktop-service-type)
                  (service bluetooth-service-type)
                  (service thermald-service-type)
                  ;; For Nintendo Switch Pro controller
                  (service joycond-service-type)
                  (service cups-service-type
                           (cups-configuration
                            (web-interface? #t)
                            (extensions
                             (list (specification->package "cups-filters")
                                   ;; Brother laser printers
                                   (specification->package "brlaser")))))
                  ;; Used by GNOME Boxes
                  (service libvirt-service-type)
                  (service virtlog-service-type)
                  ;; The first entry increases the maximum realtime
                  ;; priority for non-privileged processes; the second
                  ;; entry lifts any restriction of the maximum
                  ;; address space that can be locked in memory.
                  (service pam-limits-service-type
        		   (list
        		    (pam-limits-entry "@realtime" 'both 'rtprio 99)
        		    (pam-limits-entry "@realtime" 'both 'memlock
                                              'unlimited)))
                  %desktop-services)))
      (modify-services services
        ;; No pulseaudio.  We run pipewire as a home service instead.
        (delete pulseaudio-service-type)
        ;; Use Wayland instead of X11
        (gdm-service-type config =>
                          (gdm-configuration
                           (inherit config)
                           (wayland? #t)))
        ;; Get nonguix substitutes.
        (guix-service-type config =>
                           (guix-configuration
                            (inherit config)
                            (substitute-urls
                             (cons "https://substitutes.nonguix.org"
                                   %default-substitute-urls))
                            (authorized-keys
                             (cons (local-file "../../keys/nonguix-signing-key.pub")
                                   %default-authorized-guix-keys)))))))
   (mapped-devices (list (mapped-device
                          (source (uuid "02b1ffb4-d868-4e5f-ab9b-8be3092e3a3c"))
                          (target "cryptroot")
                          (type luks-device-mapping))))
   (file-systems (cons* (file-system
                         (mount-point "/boot/efi")
                         (device (uuid "91D3-F76B" 'fat32))
                         (type "vfat"))
                        (file-system
                         (mount-point "/")
                         (device "/dev/mapper/cryptroot")
                         (type "ext4")
                         (dependencies mapped-devices))
                        %base-file-systems))
   (name-service-switch %mdns-host-lookup-nss)))

(define-public ikaruga-home
  (home-environment
   (packages
    (cons* (package-with-pw-jack (specification->package "ardour")
                                 "ardour8")
           (package-with-pw-wrapper (specification->package "firefox")
                                    "firefox")
           (specifications->packages
            '("blender"
              "calf"
              "dragonfly-reverb"
              "easyeffects"
              "emacs"
              "emacs-better-defaults"
              "emacs-buffer-env"
              "emacs-doom-modeline"
              "emacs-doom-themes"
              "emacs-flycheck-guile"
              "emacs-geiser-guile"
              "emacs-htmlize"
              "emacs-js2-mode"
              "emacs-magit"
              "emacs-magit-annex"
              "emacs-markdown-mode"
              "emacs-org-reveal"
              "emacs-paredit"
              "emacs-racket-mode"
              "emacs-rainbow-delimiters"
              "emacs-smex"
              "emacs-typo"
              "emacs-use-package"
              "emacs-web-mode"
              "emacs-which-key"
              "emacs-yaml-mode"
              "font-google-noto-emoji"
              "font-google-noto-sans-cjk"
              "font-google-noto-serif-cjk"
              "font-inconsolata"
              "gimp"
              "git"
              "git:send-email"
              "git-annex"
              "gnome-boxes"
              "gnome-tweaks"
              "gnupg"
              "google-chrome-unstable"
              "gst-plugins-bad"
              "guile@3"
              "guitarix"
              "htop"
              "hydrogen"
              "keepassxc"
              "libreoffice"
              "libresprite"
              "lsp-plugins"
              "milkytracker"
              "ncurses"
              "obs"
              "openssh"
              "pavucontrol"
              "pinentry"
              ;; "prismlauncher"
              "qpwgraph"
              "quodlibet"
              "sfxr"
              "sicp"
              "steam"
              "strace"
              "tiled"
              "tor"
              "vlc"
              "x42-plugins"
              "xdg-desktop-portal@1.16"
              "xdg-desktop-portal-gtk"
              "xournal"
              "yt-dlp"))))
   (services (cons (service home-pipewire-service-type)
                   common-services))))
