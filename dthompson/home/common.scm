;;; Copyright © 2023 David Thompson <dthompson2@worcester.edu>
;;;
;;; This program is free software; you can redistribute it and/or
;;; modify it under the terms of the GNU General Public License as
;;; published by the Free Software Foundation; either version 3 of the
;;; License, or (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see
;;; <http://www.gnu.org/licenses/>.

(define-module (dthompson home common)
  #:use-module (gnu home)
  #:use-module (gnu packages)
  #:use-module (gnu services)
  #:use-module (guix channels)
  #:use-module (guix gexp)
  #:use-module (gnu home services)
  #:use-module (gnu home services desktop)
  #:use-module (gnu home services guix)
  #:use-module (gnu home services shells))

(define (dotfiles . file-names)
  (define dir (getcwd))
  (map (lambda (file-name)
         (list file-name
               (local-file (string-append dir "/dotfiles/" file-name)
                           (string-append "dotfile-" (basename file-name)))))
       file-names))

(define-public common-services
  (list (service home-dbus-service-type)
        (service home-bash-service-type
                 (home-bash-configuration
                  (aliases '(("grep" . "grep --color=auto")
                             ("ls" . "ls -ahlp --color=auto")))))
        (simple-service 'dotfiles
                        home-files-service-type
                        (dotfiles ".emacs.d/init.el" ".guile" ".ssh/config"))
        (simple-service 'channels
                        home-channels-service-type
                        (list
                         (channel
                          (name 'nonguix)
                          (url "https://gitlab.com/nonguix/nonguix")
                          (introduction
                           (make-channel-introduction
                            "897c1a470da759236cc11798f4e0a5f7d4d59fbc"
                            (openpgp-fingerprint
                             "2A39 3FFF 68F4 EF7A 3D29  12AF 6F51 20A0 22FB B2D5"))))
                         (channel
                          (name 'guix-gaming-games)
                          (url "https://gitlab.com/guix-gaming-channels/games.git")
                          ;; Enable signature verification:
                          (introduction
                           (make-channel-introduction
                            "c23d64f1b8cc086659f8781b27ab6c7314c5cca5"
                            (openpgp-fingerprint
                             "50F3 3E2E 5B0C 3D90 0424  ABE8 9BDC F497 A4BB CC7F"))))
                         (channel
                          (name 'dthompson)
                          (url "https://git.dthompson.us/guix-channel.git")
                          (branch "main")
                          (introduction
                           (make-channel-introduction
                            "df2e993dbfe9e45e8ad66226d3a136e3ffcbebf8"
                            (openpgp-fingerprint
                             "8CCB A7F5 52B9 CBEA E1FB  2915 8328 C747 0FF1 D807"))))
                         (channel
                          (name 'spritely)
                          (url "https://gitlab.com/spritely/spritely-guix.git")
                          (branch "main")
                          (introduction
                           (make-channel-introduction
                            "fc1a248dcc8d1da6f0460d4b03d8a6a304ccae73"
                            (openpgp-fingerprint
                             "973D E54C 08E4 ECBB A7AE  2912 8EDC 92AC 17DE 7691"))))))))
