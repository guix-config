;; Snarfed from David Wilson who I believe snarfed from Andrew Tropin.
(define-module (dthompson home pipewire)
  #:use-module (guix build-system trivial)
  #:use-module (guix packages)
  #:use-module (gnu packages)
  #:use-module (gnu packages bash)
  #:use-module (gnu packages linux)
  #:use-module (gnu services)
  #:use-module (gnu services configuration)
  #:use-module (gnu home services)
  #:use-module (gnu home services shepherd)
  #:use-module (guix gexp)
  #:export (package-with-pw-jack
            package-with-pw-wrapper))

(define (package-with-pw-jack pkg program)
  (package
    (name (package-name pkg))
    (version (package-version pkg))
    (source #f)
    (build-system trivial-build-system)
    (arguments
     (list
      #:modules '((guix build union)
                  (guix build utils))
      #:builder
      #~(begin
          (use-modules (ice-9 match)
                       (guix build union)
                       (guix build utils))
          (let* ((program (string-append #$output "/bin/" #$program))
                 (program* (string-append program ".real"))
                 (desktop (string-append #$output "/share/applications/"
                                         #$program ".desktop")))
            (union-build #$output (list #$pkg)
                         #:create-all-directories? #t)
            (when (file-exists? desktop)
              (substitute* desktop
                (("Exec=.*")
                 (string-append "Exec=" program "\n"))))
            (rename-file program program*)
            (call-with-output-file program
              (lambda (port)
                (display "#!" port)
                (display #$bash-minimal port)
                (display "/bin/sh" port)
                (newline port)
                (display "exec " port)
                (display #$pipewire port)
                (display "/bin/pw-jack " port)
                (display program* port)
                (display " \"$@\"" port)
                (newline port)))
            (chmod program #x555)
            #t))))
    (synopsis (package-synopsis pkg))
    (description (package-description pkg))
    (home-page (package-home-page pkg))
    (license (package-license pkg))))

(define* (package-with-pw-wrapper pkg #:optional (program (package-name pkg)))
  (package
    (name (package-name pkg))
    (version (package-version pkg))
    (source #f)
    (build-system trivial-build-system)
    (arguments
     (list
      #:modules '((guix build union)
                  (guix build utils))
      #:builder
      #~(begin
          (use-modules (ice-9 match)
                       (guix build union)
                       (guix build utils))
          (let* ((program (string-append #$output "/bin/" #$program))
                 (desktop (string-append #$output "/share/applications/"
                                         #$program ".desktop"))
                 (pw-lib (string-append #$pipewire "/lib")))
            (union-build #$output (list #$pkg)
                         #:create-all-directories? #t)
            (when (file-exists? desktop)
              (substitute* desktop
                (("Exec=.*")
                 (string-append "Exec=" program "\n"))))
            (wrap-program program
              #:sh (string-append #$bash-minimal "/bin/sh")
              `("LD_LIBRARY_PATH" ":" prefix (,pw-lib)))))))
    (synopsis (package-synopsis pkg))
    (description (package-description pkg))
    (home-page (package-home-page pkg))
    (license (package-license pkg))))
